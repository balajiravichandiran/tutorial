import numpy as np
import cv2

print(cv2.__version__)
capture = cv2.VideoCapture(2)
capture.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
width = 1280
height = 720
capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)


while(True):
    # Capture frame-by-frame
    ret, frame = capture.read()
  
    #print(ret)
    if ret:
        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
